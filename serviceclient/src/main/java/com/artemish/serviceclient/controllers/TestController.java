package com.artemish.serviceclient.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1")
public class TestController {

	@GetMapping("/health")
	public String stateadd() throws Exception {
		return "response";

	}

	@GetMapping("/health2")
	public String stateadd2() throws Exception {
		return "response2";

	}

}
